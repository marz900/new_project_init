const COLORS = {
    white: '#fff',
    black: '#000000',
    blue: '#5D5FEE',
    grey: '#BABBC3',
    light: '#F3F4FB',
    darkBlue: '#7978B5',
    yellow: '#FCC900',
    red: 'red',
  };
  export default COLORS;