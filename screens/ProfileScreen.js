import React, {useState} from 'react';
import { View, Text, TouchableOpacity, Image, SafeAreaView, ScrollView} from 'react-native';
import COLORS from '../colors/colors';

export default function SettingsScreen({ navigation }) {
    const[defaultRating, setdefaultRating] = useState(2)
    const[maxRating, setmaxRating] = useState([1,2,3,4,5])

    const starImgFilled = 'https://github.com/tranhonghan/images/blob/main/star_filled.png'
    const starImgCorner = 'https://github.com/tranhonghan/images/blob/main/star_corner.png'

    const CustomRatingBar = () => {
        return(
            <View style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginTop: 30,
            }}>
                <Text style={{
                }}>
                {
                maxRating.map((item, index) => {
                    return(
                        <TouchableOpacity
                        activeOpacity={0.7}
                        key={item}
                        onPress={() => defaultRating(item)}
                        >
                            <Image
                            style={{
                                width: 20,
                                height: 20,
                                resizeMode: 'cover',
                                
                            }}
                            source={
                                item <= defaultRating
                                ? {uri: starImgFilled}
                                : {uri: starImgCorner}
                            }
                            />
                        </TouchableOpacity>
                        )
                    })
                }
            </Text>
            </View>
            
        )
    }
    return (
        <SafeAreaView style={{
            backgroundColor: COLORS.yellow,
            borderBottomLeftRadius: 25,
            borderBottomRightRadius: 25,
            paddingVertical: '4%'
        }}>
            <ScrollView 
                contentContainerStyle={{paddingTop: 50, paddingHorizontal: 20, justifyContent: 'center'}}
                showsVerticalScrollIndicator={false} >
                <Image source={{uri: 'https://thumbs.dreamstime.com/b/default-avatar-profile-image-vector-social-media-user-icon-potrait-182347582.jpg'}} style={{
                    justifyContent: 'center', 
                    resizeMode: 'contain', 
                    alignSelf: 'center',
                    width: 130,
                    height: 130,
                    top: '-25%',
                    borderRadius: 75,
                    }}/>
                <Text style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    fontStyle: 'normal',
                    fontSize: 30,
                    top: '-22%',
                    color: COLORS.white,
                }}>User Name</Text>
                
               
            </ScrollView>
                <CustomRatingBar/>
        </SafeAreaView>
    );
}