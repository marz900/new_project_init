import React, { useState } from 'react';
import { Alert, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import COLORS from '../colors/colors';


import Ionicons from 'react-native-vector-icons/Ionicons';


export default function CameraScreen({ navigation }) {
    const[image, setImage] = useState(null)
    const[images, setImages] = useState(null)


    const pickSingleWithCamera = (cropping, mediaType = 'photo') => {
        ImagePicker.openCamera({
          cropping: cropping,
          width: 500,
          height: 500,
          includeExif: true,
          mediaType,
        })
          .then((image) => {
            console.log('received image', image);
            setImage({
                uri: image.path,
                width: image.width,
                height: image.height,
                mime: image.mime,
            })
            setImages(null)
          })
          .catch((e) => alert(e));
      }


    const pickSingleBase64 = (cropit) => {
        ImagePicker.openPicker({
            width: 300,
            height: 300,
            cropping: cropit,
            includeBase64: true,
            includeExif: true,
        })
            .then((image) => {
            console.log('received base64 image', image);
            setImage({
                uri: `data:${image.mime};base64,` + image.data,
                width: image.width,
                height: image.height,
            })
            setImages(null)
            })
            .catch((e) => alert(e));
        }


    const renderAsset = (image) => {
        // if (image.mime && image.mime.toLowerCase().indexOf('video/') !== -1) {
        //   return this.renderVideo(image);
        // }
    
        return renderImage(image);
      }

    const renderImage = (image) => {
        return (
            <ScrollView style={{padding: '10%'}}>
                <View style={{}}>
                    <Image
                        style={{ width: 250, height: 250, resizeMode: 'contain', borderRadius: 50}}
                        source={image}
                    />
                </View>
            </ScrollView>
          
        );
      }
    
    

    return (
        <View style={styles.container}>
           <ScrollView>
            {image ? renderAsset(image) : null}
            {images
                ? images.map((i) => (
                    <View key={i.uri}>{renderAsset(i)}</View>
                ))
                : null}
            </ScrollView>

            <View style={{
                padding: '10%',
                alignItems: 'center',
                }}>
                <View style={styles.Uni}>
                    <TouchableOpacity 
                    style={{
                        justifyContent: 'center',
                        borderRadius: 50,
                        shadowOpacity: 0.29,
                        shadowRadius: 4.65,
                        elevation: 2,
                        width: 80,
                        height: 80,
                        backgroundColor: COLORS.yellow,
                        marginTop: '10%'

                    }}
                    onPress={() => pickSingleWithCamera(true)}
                    >
                        <Text style={styles.text}>
                            <Ionicons name="camera-outline" color={COLORS.black} size={50}/>
                        </Text>
                        </TouchableOpacity>
                </View>

                <View style={styles.Uni}>
                    <TouchableOpacity 
                    style={{
                        justifyContent: 'center',
                        borderRadius: 30,
                        shadowOpacity: 0.29,
                        shadowRadius: 4.65,
                        elevation: 2,
                        padding: 10,
                        width: 200,
                        height: 50,
                        backgroundColor: COLORS.yellow,
                        marginTop: '10%'
                    }}
                    onPress={() => pickSingleBase64(false)}
                    >
                        <Text style={styles.text}>
                            select image
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      
    },
    Uni: {
        
    },
    text: {
      color: 'white',
      fontSize: 20,
      textAlign: 'center',
      textTransform: 'uppercase'
    },
  });