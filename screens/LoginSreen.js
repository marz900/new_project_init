import React from 'react';
import {View, Text, SafeAreaView, Keyboard, Alert, TouchableOpacity, Image} from 'react-native';
import Input from '../components/Inputs';
import Button from '../components/Button';
import COLORS from '../colors/colors';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import Loader from '../components/Loader';
import  AsyncStorage  from 'react-native';
import TabNavigator from '../navigation/TabNavigator';

const LoginScreen = ({navigation}) => {
  const [inputs, setInputs] = React.useState({email: '', password: ''});
  const [errors, setErrors] = React.useState({});
  const [loading, setLoading] = React.useState(false);

  const validate = async () => {
    Keyboard.dismiss();
    let isValid = true;
    if (!inputs.email) {
      handleError('Please input email', 'email');
      isValid = false;
    }
    if (!inputs.password) {
      handleError('Please input password', 'password');
      isValid = false;
    }
    if (isValid) {
      login();
    }
  };

  const login = () => {
    setLoading(true);
    setTimeout(async () => {
      setLoading(false);
      let userData = await AsyncStorage.getItem('userData');
      if (userData) {
        userData = JSON.parse(userData);
        if (
          inputs.email == userData.email &&
          inputs.password == userData.password
        ) {
          navigation.navigate('HomeScreen');
          AsyncStorage.setItem(
            'userData',
            JSON.stringify({...userData, loggedIn: true}),
          );
        } else {
          Alert.alert('Error', 'Invalid Details');
        }
      } else {
        Alert.alert('Error', 'User does not exist');
      }
    }, 3000);
  };

  const handleOnchange = (text, input) => {
    setInputs(prevState => ({...prevState, [input]: text}));
  };

  const handleError = (error, input) => {
    setErrors(prevState => ({...prevState, [input]: error}));
  };
  return (
    <SafeAreaView style={{backgroundColor: COLORS.white, flex: 1}}>
      <Loader visible={loading} />
      <View style={{paddingTop: 50, paddingHorizontal: 20}}>
      <Image source={require('../assets/MC.png')} style={{
            justifyContent: 'center', 
            resizeMode: 'contain', 
            alignSelf: 'center',
            width: 150,
            height: 150
            }}/>
        <Text style={{color: COLORS.yellow, fontSize: 40, marginVertical: 10, textAlign: 'center'}}>
          Welcome to MC
        </Text>
        <Text style={{color: COLORS.black, fontSize: 18, marginVertical: 10, textAlign: 'center'}}>
          Mobile Mechanic Services
        </Text>
        <View style={{marginVertical: 30}}>
          <Input
            onChangeText={text => handleOnchange(text, 'email')}
            onFocus={() => handleError(null, 'email')}
            iconName="email-outline"
            // label="Email"
            placeholder="Email"
            error={errors.email}
          />
          <Input
            onChangeText={text => handleOnchange(text, 'password')}
            onFocus={() => handleError(null, 'password')}
            iconName="lock-outline"
            // label="Password"
            placeholder="Password"
            error={errors.password}
            password
          />
          <Text style={{
            fontWeight: '700',
            fontSize: 15,
            fontStyle: 'italic',
            alignItems: 'flex-end'
          }}>Forgot Password?</Text>
          <TouchableOpacity onPress={validate} style={{
              backgroundColor: COLORS.black,
              padding: '4%',
              borderRadius: 25,
              width: '80%',
              alignSelf: 'center',
          }}>
              <Text style={{
                  textAlign: 'center',
                  fontSize: 18,
                  fontWeight: 'bold',
                  color: COLORS.light
                
                  }}>
                  Login
              </Text>
          </TouchableOpacity>
          <Text
            onPress={() => navigation.navigate('HomeScreen')}
            style={{
              color: COLORS.black,
              fontWeight: 'bold',
              textAlign: 'center',
              fontSize: 16,
              paddingVertical: '15%'
            }}>
            Don't have an account?{'\t'}
            <Text style={{
              color: COLORS.yellow,
            }}>
              Sign Up
            </Text>
          </Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default LoginScreen;