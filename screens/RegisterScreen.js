import  AsyncStorage  from 'react-native';
import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Keyboard,
  ScrollView,
  Alert,
  TouchableOpacity,
  Image
} from 'react-native';

import COLORS from '../colors/colors';
import Button from '../components/Button';
import Input from '../components/Inputs';
import Loader from '../components/Loader';

const RegisterScreen = ({navigation}) => {
  const [inputs, setInputs] = React.useState({
    email: '',
    fullname: '',
    // phone: '',
    password: '',
    re_password: '',
  });
  const [errors, setErrors] = React.useState({});
  const [loading, setLoading] = React.useState(false);

  const validate = () => {
    Keyboard.dismiss();
    let isValid = true;

    if (!inputs.email) {
      handleError('Please input email', 'email');
      isValid = false;
    } else if (!inputs.email.match(/\S+@\S+\.\S+/)) {
      handleError('Please input a valid email', 'email');
      isValid = false;
    }

    if (!inputs.fullname) {
      handleError('Please input fullname', 'fullname');
      isValid = false;
    }

    // if (!inputs.phone) {
    //   handleError('Please input phone number', 'phone');
    //   isValid = false;
    // }

    if (!inputs.password) {
      handleError('Please input password', 'password');
      isValid = false;
    } else if (inputs.password.length < 5) {
      handleError('Min password length of 5', 'password');
      isValid = false;
    }

    if (!inputs.re_password) {
      handleError('Please input re-type password', 're_password');
      isValid = false;
    } else if (inputs.password.length < 5) {
      handleError('Min password length of 5', 're_password');
      isValid = false;
    }

    if (isValid) {
      register();
    }
  };

  const register = () => {
    setLoading(true);
    setTimeout(() => {
      try {
        setLoading(false);
        AsyncStorage.setItem('userData', JSON.stringify(inputs));
        console.log('LLLLLLLL') 
        navigation.navigate('LoginScreen');
      } catch (error) {
          console.log('MMMMMMMMMMMM', inputs)
        Alert.alert('Error', 'Something went wrong');
      }
    }, 3000);
  };

  const handleOnchange = (text, input) => {
    setInputs(prevState => ({...prevState, [input]: text}));
  };
  const handleError = (error, input) => {
    setErrors(prevState => ({...prevState, [input]: error}));
  };
  return (
    <SafeAreaView style={{backgroundColor: COLORS.white, flex: 1, paddingHorizontal: 15}}>
      <Loader visible={loading} />
      <ScrollView
        contentContainerStyle={{paddingTop: 50, paddingHorizontal: 20, justifyContent: 'center'}}>
        <Image source={require('../assets/MC.png')} style={{
            justifyContent: 'center', 
            resizeMode: 'contain', 
            alignSelf: 'center',
            width: 150,
            height: 150
            }}/>
        <View style={{marginVertical: 13}}>
          <Input
            onChangeText={text => handleOnchange(text, 'fullname')}
            onFocus={() => handleError(null, 'fullname')}
            iconName="account-outline"
            // label="Email"
            placeholder="User Name"
            error={errors.fullname}
          />

          <Input
            onChangeText={text => handleOnchange(text, 'email')}
            onFocus={() => handleError(null, 'email')}
            iconName="email-outline"
            // label="Full Name"
            placeholder="Email"
            error={errors.email}
          />

          {/* <Input
            keyboardType="numeric"
            onChangeText={text => handleOnchange(text, 'phone')}
            onFocus={() => handleError(null, 'phone')}
            iconName="phone-outline"
            label="Phone Number"
            placeholder="Password"
            error={errors.phone}
          /> */}
          <Input
            onChangeText={text => handleOnchange(text, 'password')}
            onFocus={() => handleError(null, 'password')}
            iconName="lock-outline"
            // label="Password"
            placeholder="Password"
            error={errors.password}
            password
          />

          <Input
            onChangeText={text => handleOnchange(text, 're_password')}
            onFocus={() => handleError(null, 're_password')}
            iconName="lock-outline"
            // label="Password"
            placeholder="Re-type Password"
            error={errors.re_password}
            password
          />
          <TouchableOpacity onPress={validate} style={{
              backgroundColor: COLORS.black,
              padding: '4%',
              borderRadius: 25,
              width: '80%',
              alignSelf: 'center',
          }}>
              <Text style={{
                  textAlign: 'center',
                  fontSize: 18,
                  fontWeight: 'bold',
                  color: COLORS.light
                
                  }}>
                  Sign Up
              </Text>
          </TouchableOpacity>
          <Text
            onPress={() => navigation.navigate('LoginScreen')}
            style={{
              color: COLORS.black,
              fontWeight: '800',
              textAlign: 'center',
              fontSize: 16,
            }}>
            Already have an account ? Sign In
          </Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default RegisterScreen;