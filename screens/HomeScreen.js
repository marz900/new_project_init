import React, {useState} from 'react';
import { View, Text, TouchableOpacity, Image, SafeAreaView, ScrollView, TextInput, StyleSheet} from 'react-native';
import COLORS from '../colors/colors';


import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconFeather from 'react-native-vector-icons/Feather';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default function DetailsScreen({ navigation }) {
    const[defaultRating, setdefaultRating] = useState(5)
    const[maxRating, setmaxRating] = useState([1,2,3,4,5])

    const starImgFilled = 'https://raw.githubusercontent.com/tranhonghan/images/main/star_filled.png'
    const starImgCorner = 'https://raw.githubusercontent.com/tranhonghan/images/main/star_corner.png'


    const CustomRatingBar = () => {
        return(
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginBottom: 20,
                }}>
                <View style={{
                    flexDirection: 'row',
                    

                }}>
                    <Image
                    style={{
                        height: 55,
                        width: 55,
                        borderRadius: 50,
                        tintColor: COLORS.yellow
                    }}
                    source={{uri: 'https://thumbs.dreamstime.com/b/default-avatar-profile-image-vector-social-media-user-icon-potrait-182347582.jpg'}}
                    />
                    <View style={{
                        flexDirection: 'column',
                        alignSelf: 'center'
                    }}>
                        <Text style={{
                            fontSize: 15, 
                            fontFamily: 'Roboto-Medium', 
                            color: COLORS.light,
                            paddingLeft: '2%',
                            }}>
                            Hello John Doe
                        </Text>
                        <Text style={{
                            paddingLeft: '3%',
                            
                            }}>
                        {
                            maxRating.map((item, index) => {
                                return(
                                    <TouchableOpacity
                                    activeOpacity={0.7}
                                    key={item}
                                    onPress={() => setdefaultRating(item)}
                                    >
                                        <Image
                                        style={{
                                            width: 20,
                                            height: 20,
                                            resizeMode: 'cover',
                                            
                                        }}
                                        source={
                                            item <= defaultRating
                                            ? {uri: starImgFilled}
                                            : {uri: starImgCorner}
                                        }
                                        />
                                    </TouchableOpacity>
                                    )
                                })
                            }
                        </Text>
                    </View>
                    
                </View>
                
                <TouchableOpacity onPress={() => {}}>
                    <IconFontAwesome name="map-marker" color={COLORS.yellow} size={30} />
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <><SafeAreaView style={{
            backgroundColor: COLORS.black,
            height: '14%'
        }}>
            <ScrollView style={{
                padding: 20,
            }}>
                <CustomRatingBar />
            </ScrollView>

        </SafeAreaView>
        <SafeAreaView style={{
            backgroundColor: COLORS.light,
            height: '10%',
            borderBottomColor: COLORS.black,
            borderBottomWidth: 1,
        }}>
            <ScrollView style={{
            }}
            showsVerticalScrollIndicator={false}
            >
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center'
                }}>
                    <Text style={styles.Uniform} onPress={() => {}}>
                        All
                    </Text>
                    <Text style={styles.Uniform} onPress={() => {}}>
                        <IconFontAwesome name="map-marker" color={COLORS.yellow} size={25} />{'\t'}Set Location
                    </Text>
                    <Text style={[styles.Uniform, {}]} onPress={() => {}}>
                        <IconMaterialIcons name="schedule" color={COLORS.yellow} size={25} />{'\t'}Schedules
                    </Text>
                </View>
            </ScrollView>
        </SafeAreaView>
        <ScrollView style={{
            padding: 10
        }}>
            <View>
                <Text style={{
                    fontSize: 20,
                    fontWeight: 'bold',
                    fontStyle: 'normal'
                }}>
                    Buyer Requests
                </Text>
            </View>
            <View style={{
                padding: 10,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center'
            }}>
                <Text style={[styles.Uniform1, {}]} onPress={() => {}}>
                    Filter Issues{'\t'}<Ionicons name="ios-chevron-down-circle" color={COLORS.black} size={18} />
                </Text>
                <Text style={[styles.Uniform1, {}]} onPress={() => {}}>
                    All{'\t'}<Ionicons name="ios-chevron-down-circle" color={COLORS.black} size={18} />
                </Text>
            </View>
        </ScrollView>
        </>
    );
}

const styles = StyleSheet.create({
    Uniform: {
        fontSize: 20,
        fontWeight: 'bold',
        fontStyle: 'normal',
        paddingVertical: '4%'
    },
    Uniform1: {
        fontSize: 18,
        fontWeight: 'bold',
        fontStyle: 'italic',
        paddingVertical: '4%'
    }

})