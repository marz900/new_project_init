import React, {useState} from 'react';
import { View, Text, TouchableOpacity, Image, SafeAreaView, ScrollView} from 'react-native';
import COLORS from '../colors/colors';

export default function ChatScreen({ navigation }) {
    const[defaultRating, setdefaultRating] = useState(2)
    const[maxRating, setmaxRating] = useState([1,2,3,4,5])

    const starImgFilled = 'https://raw.githubusercontent.com/tranhonghan/images/main/star_filled.png'
    const starImgCorner = 'https://raw.githubusercontent.com/tranhonghan/images/main/star_corner.png'

    const CustomRatingBar = () => {
        return(
            <View style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginTop: 30,
            }}>
                <Text style={{
                }}>
                {
                maxRating.map((item, index) => {
                    return(
                        <TouchableOpacity
                        activeOpacity={0.7}
                        key={item}
                        onPress={() => setdefaultRating(item)}
                        >
                            <Image
                            style={{
                                width: 20,
                                height: 20,
                                resizeMode: 'cover',
                                
                            }}
                            source={
                                item <= defaultRating
                                ? {uri: starImgFilled}
                                : {uri: starImgCorner}
                            }
                            />
                        </TouchableOpacity>
                        )
                    })
                }
            </Text>
            </View>
            
        )
    }
    return (
        <View style={{
            flex: 1,
            justifyContent: 'center',
            margin: 10
        }}>
            <Text
            style={{
                fontSize: 30,
                textAlign: 'center'
            }}
            >Rating Screen
            </Text>
            <Text><CustomRatingBar/></Text>
            
        </View>
    );
}