import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import  AsyncStorage  from 'react-native';
import LoginScreen from '../screens/LoginSreen';
import RegisterScreen from '../screens/RegisterScreen';
import HomeScreen from '../screens/HomeScreen';
import Loader from '../components/Loader';

import TabNavigator from './TabNavigator';



const Stack = createNativeStackNavigator();



const AuthStacks = () => {
    const [initialRouteName, setInitialRouteName] = React.useState('');
  
    React.useEffect(() => {
      setTimeout(() => {
        authUser();
      }, 2000);
    }, []);
  
    const authUser = async () => {
      try {
        let userData = await AsyncStorage.getItem('userData');
        if (userData) {
          userData = JSON.parse(userData);
          if (userData.loggedIn) {
            setInitialRouteName('TabNavigator');
          } else {
            setInitialRouteName('LoginScreen');
          }
        } else {
          setInitialRouteName('RegisterScreen');
        }
      } catch (error) {
        setInitialRouteName('RegisterScreen');
      }
    };
  
    return (
        <>
        {!initialRouteName ? (
          <Loader visible={true} />
        ) : (
          <>
            <Stack.Navigator
              initialRouteName={initialRouteName}
              screenOptions={{headerShown: false}}>
              <Stack.Screen
                name="RegisterScreen"
                component={RegisterScreen}
              />
              <Stack.Screen name="LoginScreen" component={LoginScreen} />
              <Stack.Screen name="HomeScreen" component={HomeScreen} />
            </Stack.Navigator>
          </>
        )}
        </>
    );
  };
  
export default AuthStacks;