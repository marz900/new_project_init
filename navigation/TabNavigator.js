import React from 'react';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import HomeScreen from '../screens/HomeScreen';
import ProfileScreen from '../screens/ProfileScreen';
import CameraScreen from '../screens/CameraScreen';
import ChatScreen from '../screens/ChatScreen';
import OrderScreen from '../screens/OrderScreen';


import Ionicons from 'react-native-vector-icons/Ionicons';
import IconFeather from 'react-native-vector-icons/Feather';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import COLORS from '../colors/colors';


const Tab = createBottomTabNavigator();

const TabNavigator = () => {
    return(
        <Tab.Navigator screenOptions={{
            headerShown: false,
            tabBarShowLabel: false,
            tabBarStyle: {backgroundColor: COLORS.light},
            tabBarInactiveTintColor: COLORS.black,
            tabBarActiveTintColor: COLORS.yellow,
        }} >
            <Tab.Screen name='Home' component={HomeScreen} options={{
                tabBarIcon: ({color, size}) => (
                <Ionicons name="home-outline" color={color} size={size} />
                ),
            }}/>
            <Tab.Screen name='Chat' component={ChatScreen} options={{
                tabBarIcon: ({color, size}) => (
                    <MaterialCommunityIcons name="chat-processing-outline" color={color} size={size} />
                    ),
            }} />
            <Tab.Screen name='Camera' component={CameraScreen} options={{
                tabBarIcon: ({color, size}) => (
                    <IconFeather name="camera" color={color} size={size} />
                    ),
            }} />
            <Tab.Screen name='Orders' component={OrderScreen} options={{
                tabBarIcon: ({color, size}) => (
                    <IconAntDesign name="profile" color={color} size={size} />
                    ),
            }} />
            <Tab.Screen name='Profile' component={ProfileScreen} options={{
                tabBarIcon: ({color, size}) => (
                    <IconFontAwesome name="user-o" color={color} size={size} />
                    ),
            }} />
        </Tab.Navigator>
    )
}

export default TabNavigator;